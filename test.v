module test();

	reg clk, reset, button0, button1;
	reg [7:0] sw;
	
	wire [6:0] hex0, hex1, hex2, hex3;
	
	integer i;
	
	
	defparam m0.SEKUNDA = 3;
	main m0(
		.clk(clk),
		.reset(reset),
		.button0(button0),
		.button1(button1),
		.sw(sw),
		
		.hex0(hex0),
		.hex1(hex1),
		.hex2(hex2),
		.hex3(hex3)
	);
	
	
	
	initial begin
		$dumpfile("dump.vcd");
		$dumpvars(0, test);
		
		clk = 0;
		reset = 0;
		button0 = 0;
		button1 = 0;
		
		sw = 0;
		
		i = 0;
	end
	
	always #1 clk = ~clk;
	
	initial begin
		#4 reset = 1;
		#4 reset = 0;
		
		//five digits
		#4 sw[0] = 1;
		#4 button0 = 1;
		#4 button0 = 0;
		
		#4 sw[1] = 1;
		#4 button0 = 1;
		#4 button0 = 0;
		
		#4 sw[4] = 1;
		#4 button0 = 1;
		#4 button0 = 0;
		
		#4 sw[0] = 0;
		#4 button0 = 1;
		#4 button0 = 0;
		
		#4 sw[7] = 1;
		#4 button0 = 1;
		#4 button0 = 0;
		
		//lock
		#10 button0 = 1;
		#40	button0 = 0;
		//locked

		for(i = 1; i <= 10; i = i + 1) begin
			#2 sw = 8'h00;
		
			//five digits
			#4 button0 = 1;
			#4 button0 = 0;
			
			#4 sw[2] = 1;
			#4 button0 = 1;
			#4 button0 = 0;
			
			#4 sw[5] = 1;
			#4 button0 = 1;
			#4 button0 = 0;
			
			#4 sw[2] = 0;
			#4 button0 = 1;
			#4 button0 = 0;
			
			#4 sw[6] = 1;
			#4 button0 = 1;
			#4 button0 = 0;
			//test
			#10 button0 = 1;
			#40	button0 = 0;
			
			if(i % 5 == 0) begin
				#4 button1 = 1;
				#4 button1 = 0;
				
				//wait
				#80 sw = 8'h00;
			end
		end
		
		#2 sw = 8'h00;
		#4 reset = 1;
		#4 reset = 0;
		
		//ten digits
		#4 sw[3] = 1;
		#4 button0 = 1;
		#4 button0 = 0;
		
		#4 sw[1] = 1;
		#4 button0 = 1;
		#4 button0 = 0;
		
		#4 sw[4] = 1;
		#4 button0 = 1;
		#4 button0 = 0;
		
		#4 sw[1] = 0;
		#4 button0 = 1;
		#4 button0 = 0;
		
		#4 sw[7] = 1;
		#4 button0 = 1;
		#4 button0 = 0;
		
		#4 sw[0] = 1;
		#4 button0 = 1;
		#4 button0 = 0;
		
		#4 sw[6] = 1;
		#4 button0 = 1;
		#4 button0 = 0;
		
		#4 sw[3] = 0;
		#4 button0 = 1;
		#4 button0 = 0;
		
		#4 sw[0] = 0;
		#4 button0 = 1;
		#4 button0 = 0;
		
		#4 sw[5] = 1;
		#4 button0 = 1;
		#4 button0 = 0;
		
		//lock
		#10 button0 = 1;
		#40	button0 = 0;
		//locked
		
		for(i = 1; i <= 15; i = i + 1) begin
			
			#6 sw = 8'h00;
			
			//ten digits
			#4 button0 = 1;
			#4 button0 = 0;
			
			#4 sw[2] = 1;
			#4 button0 = 1;
			#4 button0 = 0;
			
			#4 sw[5] = 1;
			#4 button0 = 1;
			#4 button0 = 0;
			
			#4 sw[2] = 0;
			#4 button0 = 1;
			#4 button0 = 0;
			
			#4 sw[6] = 1;
			#4 button0 = 1;
			#4 button0 = 0;
			
			#4 sw[2] = 1;
			#4 button0 = 1;
			#4 button0 = 0;
			
			#4 sw[5] = 0;
			#4 button0 = 1;
			#4 button0 = 0;
			
			#4 sw[2] = 1;
			#4 button0 = 1;
			#4 button0 = 0;
			
			#4 sw[3] = 0;
			#4 button0 = 1;
			#4 button0 = 0;
			
			#4 sw[0] = 0;
			#4 button0 = 1;
			#4 button0 = 0;
			
			//test
			#10 button0 = 1;
			#40	button0 = 0;
			
			//show sequence
			if(i % 5 == 0) begin
				#4 button1 = 1;
				#4 button1 = 0;
				
				//wait
				#120 sw = 8'h00;
			end
		end
		
		#2 sw = 8'h00;
		//correct sequence
		
		#4 sw[3] = 1;
		#4 button0 = 1;
		#4 button0 = 0;
		
		#4 sw[1] = 1;
		#4 button0 = 1;
		#4 button0 = 0;
		
		#4 sw[4] = 1;
		#4 button0 = 1;
		#4 button0 = 0;
		
		#4 sw[1] = 0;
		#4 button0 = 1;
		#4 button0 = 0;
		
		#4 sw[7] = 1;
		#4 button0 = 1;
		#4 button0 = 0;
		
		#4 sw[0] = 1;
		#4 button0 = 1;
		#4 button0 = 0;
		
		#4 sw[6] = 1;
		#4 button0 = 1;
		#4 button0 = 0;
		
		#4 sw[3] = 0;
		#4 button0 = 1;
		#4 button0 = 0;
		
		#4 sw[0] = 0;
		#4 button0 = 1;
		#4 button0 = 0;
		
		#4 sw[5] = 1;
		#4 button0 = 1;
		#4 button0 = 0;
		
		//unlock
		#10 button0 = 1;
		#40	button0 = 0;
		
		#20 $finish;
	end
		
endmodule
