module bin_to_ss(
	input wire [3:0] input_value,
	output reg [6:0] output_value
);

	always @ (*) begin
		case(input_value)
			4'h0:	output_value = 7'b100_0000;
			4'h1:	output_value = 7'b111_1001;
			4'h2:	output_value = 7'b010_0100;
			4'h3:	output_value = 7'b011_0000;
			4'h4:	output_value = 7'b001_1001;
			4'h5:	output_value = 7'b001_0010;
			4'h6:	output_value = 7'b000_0010;
			4'h7:	output_value = 7'b111_1000;
			4'h8:	output_value = 7'b000_0000;
			4'h9:	output_value = 7'b001_1000;
			4'hA:output_value = 7'b100_0111;
		default:	output_value = 7'b111_1111;
			
		
		endcase
	end


endmodule

