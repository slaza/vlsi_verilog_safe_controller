module main(
	input wire clk,
	input wire reset,
	input wire [7:0] sw,
	
	input wire button0,
	input wire button1,
	
	output wire [6:0] hex0,
	output wire [6:0] hex1,
	output wire [6:0] hex2,
	output wire [6:0] hex3
	
);
	
	localparam DISP_OFF = 4'hF;
	localparam L_VALUE = 4'hA;
	
	reg [7:0] value_reg, value_next;
	reg [3:0] disp0, disp1, disp2, disp3;
	
	//2
	parameter SEKUNDA = 50_000_000;
	parameter DVE_SEKUNDE = SEKUNDA * 2;
	parameter TRI_SEKUNDE = SEKUNDA * 3;
	
	localparam STARTNO_STANJE = 4'h0;
	localparam ZAKLJUCANO_STANJE = 4'h1;
	localparam PRIKAZ_STANJE = 4'h2;
	localparam OTKLJUCANO_STANJE = 4'h3;
	
	reg [3:0] state_reg, state_next;
	
	integer counter_reg, counter_next;
	wire red_b0, red_b1;
	
	reg [7:0] saved_values_reg [7:0];
	reg [7:0] saved_values_next [7:0];
	
	reg [3:0] saved_count_reg, saved_count_next;
	
	reg [4:0] index_prikazanog_reg, index_prikazanog_next;
	
	//3
	reg [7:0] test_sequence_reg [7:0];
	reg [7:0] test_sequence_next [7:0];
	
	integer i;
	/////////////////////////////
	bin_to_ss ss0(
		.input_value(disp0),
		.output_value(hex0)
	);
	
	bin_to_ss ss1(
		.input_value(disp1),
		.output_value(hex1)
	);
	
	bin_to_ss ss2(
		.input_value(disp2),
		.output_value(hex2)
	);
	
	bin_to_ss ss3(
		.input_value(disp3),
		.output_value(hex3)
	);
	/////////////////////////////
	red red0(
		.clk(clk),
		.reset(reset), 
		.input_value(button0),
		.output_value(red_b0)
	);
	red red1(
		.clk(clk),
		.reset(reset),
		.input_value(button1),
		.output_value(red_b1)
	
	);
	
	/////////////////////////////
	always @ (posedge clk, posedge reset) begin
		if(reset) begin
			value_reg <= 0;
			//2
			state_reg <= STARTNO_STANJE;
			counter_reg <= 0;
			saved_count_reg <= 0;
			index_prikazanog_reg <= 0;
			
			for(i = 0; i < 8; i = i+1) begin
				saved_values_reg[i] <= 0;
			end
			
			//3
			for(i = 0; i < 8; i = i+1) begin
				test_sequence_reg[i] <= 0;
			end
		end else begin
			value_reg <= value_next;
			//2
			state_reg <= state_next;
			counter_reg <= counter_next;
			saved_count_reg <= saved_count_next;
			index_prikazanog_reg <= index_prikazanog_next;
			
			for(i = 0; i < 8; i = i + 1) begin
				saved_values_reg[i] <= saved_values_next[i];
			end
			//3
			for(i = 0; i < 8; i = i + 1) begin
				test_sequence_reg[i] <= test_sequence_next[i];
			end
		end
	end
	
	
	always @ (*) begin
		
		state_next = state_reg;
		counter_next = counter_reg;
		saved_count_next = saved_count_reg;
	
		value_next = sw;
		index_prikazanog_next = index_prikazanog_reg;
		
		for(i = 0; i < 8; i = i + 1) begin
			saved_values_next[i] = saved_values_reg[i];
		end
		
		for(i = 0; i < 8; i = i + 1) begin
			test_sequence_next[i] = test_sequence_reg[i];
		end
		
		disp0 = DISP_OFF;
		disp1 = DISP_OFF;
		disp2 = DISP_OFF;
		disp3 = DISP_OFF;

		case(state_reg)
		
			STARTNO_STANJE:
				begin
					disp0 = value_reg % 8'd10;
					disp1 = (value_reg / 8'd10) % 8'd10;
					disp2 = (value_reg / 8'd100);
					
					//counter
					if(button0 == 1) begin
						counter_next = (counter_reg == TRI_SEKUNDE)? TRI_SEKUNDE : counter_reg + 1;
					end  else begin
						counter_next = 0;
					end
				
					if(button0 == 0 && counter_reg < TRI_SEKUNDE && counter_reg > 0) begin
						saved_count_next = (saved_count_reg == 4'h8)? 4'h8 : saved_count_reg + 4'h1;
						
						for(i = 7; i > 0; i = i - 1) begin
							saved_values_next[i] = saved_values_reg[i - 1];
						end
						saved_values_next[0] = value_reg;
					
					end else if(button0 == 0 && counter_reg == TRI_SEKUNDE) begin
						index_prikazanog_next = 0;
						counter_next = 0;
						state_next = ZAKLJUCANO_STANJE;
					end
					
				end	//STARTNO_STANJE
				
			ZAKLJUCANO_STANJE:
				begin
					disp0 = L_VALUE;
					
					//counter
					if(button0 == 1) begin
						counter_next = (counter_reg == TRI_SEKUNDE)? TRI_SEKUNDE : counter_reg + 1;
					end  else begin
						counter_next = 0;
					end
					
					
					if(button0 == 0 && counter_reg < TRI_SEKUNDE && counter_reg > 0) begin
						for(i = 7; i > 0; i = i - 1) begin
							test_sequence_next[i] = test_sequence_reg[i - 1];
						end
						test_sequence_next[0] = value_reg;
					
					
					
					//next state
					end else if(button0 == 0 && counter_reg == TRI_SEKUNDE) begin
						if(	test_sequence_reg[0] == saved_values_reg[0] && test_sequence_reg[1] == saved_values_reg[1] && 
							test_sequence_reg[2] == saved_values_reg[2] && test_sequence_reg[3] == saved_values_reg[3] && 
							test_sequence_reg[4] == saved_values_reg[4] && test_sequence_reg[5] == saved_values_reg[5] && 
							test_sequence_reg[6] == saved_values_reg[6] && test_sequence_reg[7] == saved_values_reg[7]
						) begin
								
							state_next = OTKLJUCANO_STANJE;
						end
					end
					
					if(red_b1 == 1) begin
						index_prikazanog_next = saved_count_reg;
						counter_next = 0;
						
						state_next = PRIKAZ_STANJE;
					end
					
					
					
					
					
				end	//ZAKLJUCANO_STANJE
				
			PRIKAZ_STANJE:
				begin
					
					counter_next = (counter_reg == DVE_SEKUNDE)? 0: counter_reg + 1;
					
					
					if(counter_reg > SEKUNDA) begin
						disp0 = saved_values_reg[index_prikazanog_reg - 1] % 10;
						disp1 = (saved_values_reg[index_prikazanog_reg - 1] / 10) % 10;
						disp2 = (saved_values_reg[index_prikazanog_reg - 1] / 100);
					end
					
					if(counter_reg == DVE_SEKUNDE) begin
						if(index_prikazanog_reg == 1) begin
							state_next = ZAKLJUCANO_STANJE;
						end else begin
							index_prikazanog_next = index_prikazanog_reg - 1;
						end
					end
					
				end	//PRIKAZ_STANJE
				
			OTKLJUCANO_STANJE:
				begin
					/*
						empty
					*/
				
				end	//OTKLJUCANO_STANJE
		endcase
		
	end
endmodule
















